from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from captcha.fields import CaptchaField
from django.db.models.fields import CharField
from django.utils.translation import ugettext_lazy as _

from forms import UAPhoneNumberField


class PhoneNumberField(CharField):

    description = _("Ukrainian phone number")

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 17
        super(PhoneNumberField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'form_class': UAPhoneNumberField}
        defaults.update(kwargs)
        return super(PhoneNumberField, self).formfield(**defaults)


class MyUserManager(BaseUserManager):
    def create_user(self, name, first_name, last_name, phone_number, email, date_of_birth, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not name:
            raise ValueError('Users must have a name')

        user = self.model(
            name=name,
            first_name=first_name,
            last_name=last_name,
            email=email,
            date_of_birth=date_of_birth,
            phone_number=phone_number,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, name, first_name, last_name, phone_number, email, date_of_birth, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(name, first_name=first_name, last_name=last_name, 
            phone_number=phone_number, email=email, 
            password=password, date_of_birth=date_of_birth
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    name = models.CharField(max_length=40, unique=True)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    email = models.EmailField(max_length=255)
    date_of_birth = models.DateField()
    phone_number = PhoneNumberField()
    avatar = models.ImageField(upload_to = 'media/', default="avatar.png")
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'name'
    REQUIRED_FIELDS = ['email', 'phone_number', 'date_of_birth', 'first_name', 'last_name', 'avatar']

    def get_full_name(self):
        # The user is identified by their email address
        return self.name

    def get_short_name(self):
        # The user is identified by their email address
        return self.name

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


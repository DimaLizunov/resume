# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response, redirect
from resume.models import MyUser
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import AuthenticationForm
from admin import UserCreationForm, UserChangeForm
from django.contrib.auth import login, logout
from django.views.generic import FormView, View, TemplateView, UpdateView, DetailView, DeleteView
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from forms import CaptchaForm, ContactForm
from django.template import RequestContext
from django.core.mail import send_mail, BadHeaderError
from django.core.context_processors import csrf



class HomeView(TemplateView):
    template_name = 'resume/home.html'

class InfoView(TemplateView):
    template_name = 'resume/info.html'

   
class Login(FormView):
    form_class = AuthenticationForm
    success_url = '../'
    template_name = 'resume/login.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(Login, self).form_valid(form)


class LogOut(View):
    success_url = '/regist/'
    template_name = 'resume/base.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('/login/')


class UserView(UpdateView):
    model = MyUser
    template_name = 'resume/change_info.html'
    success_url = '../../../../'
    fields = ['name', 'email', 'phone_number', 'date_of_birth', 'first_name', 'last_name', 'avatar']


def contactView(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        #Если форма заполнена корректно, сохраняем все введённые пользователем значения
        if form.is_valid():
            subject = form.cleaned_data['subject']
            sender = form.cleaned_data['sender']
            message = form.cleaned_data['message']
            copy = form.cleaned_data['copy']

            recipients = ['resume.my@yandex.ru']
            #Если пользователь захотел получить копию себе, добавляем его в список получателей
            if copy:
                recipients.append(sender)
            try:
                send_mail(subject, message, 'resume.my@yandex.ru', recipients)
            except BadHeaderError: #Защита от уязвимости
                return HttpResponse('Invalid header found')
            #Переходим на другую страницу, если сообщение отправлено
            return render(request, 'resume/thanks.html')
    else:
        #Заполняем форму
        form = ContactForm()
    #Отправляем форму на страницу
    return render(request, 'resume/contact.html', {'form': form})
   

class UserDelete(DeleteView):
    model = MyUser
    template_name = 'resume/del_user.html'
    success_url = '../../../../'


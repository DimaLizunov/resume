from django import forms
from captcha.fields import CaptchaField
import re

from django.forms.fields import CharField
from django.core.validators import EMPTY_VALUES
from django.forms import ValidationError
from django.utils.translation import ugettext_lazy as _


phone_digits = re.compile(r'^\d{3}[-]\d{7}$')


class UAPhoneNumberField(CharField):
    default_error_messages = {
        'invalid': _('Phone numbers must be in 0XX-XXXXXXX format.'),
    }

    def clean(self, value):
        super(UAPhoneNumberField, self).clean(value)
        if value in EMPTY_VALUES:
            return u''
        match = phone_digits.match(value)
        if match:
            return value
        raise ValidationError(self.error_messages['invalid'])

class CaptchaForm(forms.Form):
    captcha = CaptchaField()


class ContactForm(forms.Form):
    subject = forms.CharField(max_length = 100)
    sender = forms.EmailField()
    message = forms.CharField(widget = forms.Textarea(attrs = {'class': 'form-control'}))
    copy = forms.BooleanField(required = False)









"""my_resume URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from resume import views
from django import forms
from django.conf import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$',  views.HomeView.as_view(), name = 'home'),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^login/$',  views.Login.as_view(), name = 'login'),
    url(r'^logout/$',  views.LogOut.as_view(), name = 'logout'),
    url(r'^change_info/(?P<pk>\d+)/$',  views.UserView.as_view(), name = 'change_info'),
    url(r'^change_info/(?P<pk>\d+)/del_user/$',  views.UserDelete.as_view(), name = 'del_user'),
    url(r'^contact/$',  views.contactView, name = 'contact'),
    url(r'^info/$',  views.InfoView.as_view(), name = 'info'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
    url(r'^static/(?P<path>.*)', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT,}),
    
]
